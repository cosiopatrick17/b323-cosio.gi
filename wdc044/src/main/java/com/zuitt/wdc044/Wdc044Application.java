package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// "@SpringBootApplication" is an example of "Annotations" mark.
         // Annotations are used to provide supplemental information about the program.
        // These are used to manage and configure the behavior of the framework.
       // Annotations are used extensively in Spring Boot to configure components, define dependencies, or enable specific functionalities
      // Spring Boot scans for classes in its class path upon running and assigns behaviors on them based on their annotations.

//This specifies the main class of the springboot application

@SpringBootApplication

// This indicates that a class is a controller that will handle RESTful web requests and returns an HTTP response
@RestController
public class Wdc044Application {
// This methord Starts the whole Spring Framework.
	// This serves as the entry point to start the application.
	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}



// This is used to map HTTP request
	//Note: Http Request Annotation is usually followed by a "method body"
	// The "method body" contains the logic to generate the response.
	@GetMapping("/hello")
	// @RequestParam is used to extract query parameters from parameters, and even files from requests.
		// if the URL is : /hello?name=john the method will return Hello john
		// '?' meants that  the start of parameters followed by the key = value pair.
		// if the URL is : /hello, The method will return "Hello World"
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		// To send a response of "Hello + name"
		return String.format("Hello %s!", name);
	}
	// Activity S01
		// localhost:8080/hi?user=jane
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "Visitor") String name) {
		return String.format("Hi, %s!", name);
	}

	// Stretch Goals
		//localhost:8080/nameAge
	@GetMapping("/nameAge")
	public String nameAge(@RequestParam(value = "name", defaultValue = "Guest") String name,
						  @RequestParam(value = "age", defaultValue = "0") int age) {
		return String.format("Your name is %s, and your age is %d.", name, age);
	}
}

//@RequestMapping()